"use strict"

class Employee {
  constructor(name, age, salary){
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  set name (name){
    this._name = name;
  }
  get name(){
    return this._name;
  }

  set age (age){
    this._age = age;
  }
  get age(){
    return this._age;
  }

  set salary (salary){
    this._salary = salary;
  }
  get salary(){
    return this._salary;
  }
}


class Programmer extends Employee{
  constructor(name, age, salary, lang){
    super(name, age, salary);
    this.lang = lang;
  }

  get salaryNew() {
    return this.salary = this.salary * 3; 
  }

}

let programmer1 = new Programmer("Oleg", "26", "5000", "js");
let programmer2 = new Programmer("Alex", "29", "10000", "php");

console.log(programmer1, programmer1.salaryNew);
console.log(programmer2, programmer2.salaryNew);